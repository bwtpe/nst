String Status = "";  //DUT status 
const int RLED = A0, BLED = A1, GLED = A2;  //Pin define
const int PowerP=2, ResetP=3, AuxP=4, BtP=5, VupP=6, VdownP=7;  //Pin define
int RValue=0,BValue=0,GValue = 0;  //Color detect counting value define
char test_det ='N';  //Auto test machine status detect
char value = Serial.read();  //RS232 input read
int Total=0,
    SystemBootload=0,SystemBootloadf=0,
    NormalOperation1=0,NormalOperation1f=0,
    NormalOperation2=0,NormalOperation2f=0,
    AUX=0,AUXf=0,vupcount=0,vdowncount=0,
    SystemSleep=0,SystemSleepf=0,
    SystemPowerOff=0,SystemPowerOfff=0; //For summary data calculate
String SBF="",NO1F="",NO2F="",AUF="",SSF="",SPOF="";  //For summary data calculate
void setup() {
  Serial.begin(115200);
  
  //NST Button Pin define on EduCake
  pinMode(PowerP,OUTPUT);
  pinMode(ResetP,OUTPUT);
  pinMode(AuxP,OUTPUT);
  pinMode(BtP,OUTPUT);
  pinMode(VupP,OUTPUT);
  pinMode(VdownP,OUTPUT);
  digitalWrite(PowerP, HIGH);
  digitalWrite(ResetP, HIGH);
  digitalWrite(AuxP, HIGH);
  digitalWrite(BtP, HIGH);
  digitalWrite(VupP, HIGH);
  digitalWrite(VdownP, HIGH);

}

void loop() { 
//This Main loop is for continusly detect the user want to run or stop the test.
//If the test_det is Y,  the test will keep going for next round and the result will count up.
//If the test_det is N, the test will stop and summary the test result than clear all temp variable.
init_det();
if ((test_det != 'Y') and (test_det != 'y')){
  delay(100);
  while(test_det != 'Y'){
  if (Serial.available()){
  char value = Serial.read();
  if((value == 'N')or(value == 'n')or(value == 'Y')or(value == 'y')){
  Serial.write(value);
  Serial.println("");
  }
  if ((value != 'Y') && (value != 'y')) {
  Serial.println("Do you want to start the test?(Y/N)");
  break;}
  else if ((value == 'Y') || (value == 'y')){
  test_det = 'Y';
  }
  }
  }
}
  if (test_det == 'Y'){
    if (Total == 0) Serial.println("Test preparing......");
  Main_test_script();
  }
}
void init_det(){ //This void is for detes the user is setting the test is run or stop, if stop will call Reset_data to reset all temp variable.
if(Serial.available()){
    char value = Serial.read();
    if((value == 'N')or(value == 'n')or(value == 'Y')or(value == 'y')){
      Serial.write(value);
      Serial.println("");
      if ((value == 'N')or(value == 'n')) Reset_data();
      else if (value == 'Y') test_det = 'Y';
    }
}
}

void Reset_data(){ //Reset all temp variable
    test_det = 'N';
    if (Total != 0) Summary();
    Total=0,
    SystemBootload=0,SystemBootloadf=0,
    NormalOperation1=0,NormalOperation1f=0,
    NormalOperation2=0,NormalOperation2f=0,
    AUX=0,AUXf=0,vupcount=0,vdowncount=0,
    SystemSleep=0,SystemSleepf=0,
    SystemPowerOff=0,SystemPowerOfff=0;
    SBF="";NO1F="";NO2F="";AUF="";SSF="";SPOF="";
}

void LED_Filter(){  //For detect the LED currently color and call different function to do next calculate
  RValue = analogRead(RLED);
  BValue = analogRead(BLED);
  GValue = analogRead(GLED);
  if (RValue>200) {
  RLED_J();
  }else if (BValue>200) {
  BLED_J();
  }else if (GValue>200) {
  GLED_J();
  }else if ((RValue<200) && (BValue<200) && (GValue<200)) {
  POFF_J();
  }else {LED_Filter();
  }
}

void RLED_J(){  //Red Led color combination calculate to get DUT state
int LEDH=0,LEDL=0,GLEDH=0;
  for(int x=0;x<1000;x++){
    RValue = analogRead(RLED);
    GValue = analogRead(GLED);
    if (RValue>200) {LEDH++;} 
    else {LEDL++;}
    if (GValue>200) {GLEDH++;}
    delay(1);
  }
    if (LEDH>=900 && GLEDH>=190) {
    Status = "AUX";}
    else if (LEDH/100==3 && LEDL/100==6) {
    Status = "Sleep";} 
    else if (LEDH>100 && GLEDH == 0) {
    Status = "BootLoad";}
    else {LED_Filter();}
    
}
void BLED_J(){  //Blue Led color combination caculate to get DUT state
  LED_Filter();
}
void GLED_J(){  //Green Led color combination caculate to get DUT state
int LEDH=0,LEDL=0;
  for(int x=0;x<100;x++){
    GValue = analogRead(GLED);
    if (GValue>200) {LEDH++;} else {LEDL++;}
    delay(10);
  }
    if (LEDL>=13) {
    Status = "SoftAP mode";} 
    else if (LEDL<13) {
    Status = "Network connected";}
    else {LED_Filter();}
}
void POFF_J(){  //Tri color detect and caculate to make sure DUT is power off or not
  int RDJ=1,BDJ=1,GDJ=1;
  for(int x=0;x<100;x++){
    RValue = analogRead(RLED);
    BValue = analogRead(BLED);
    GValue = analogRead(GLED);
    if (RValue>200) {RDJ++;} 
    else if (BValue>200) {BDJ++;} 
    else if (GValue>200) {GDJ++;}
    delay(10);
  }
    if ((RDJ+BDJ+GDJ)<=3) {
    Status = "Off";}
    else {LED_Filter();}
}

void PowerB(int time){  //Power Button pressing function
 digitalWrite(PowerP,LOW);
 delay(time);
 digitalWrite(PowerP,HIGH); 
}

void ResetB(int time){  //Reset Button pressing function
 digitalWrite(ResetP,LOW);
 delay(time);
 digitalWrite(ResetP,HIGH); 
}

void AuxB(int time){  //Aux Button pressing function
 digitalWrite(AuxP,LOW);
 delay(time);
 digitalWrite(AuxP,HIGH); 
}

void BtB(int time){  //Bt Button pressing function
 digitalWrite(BtP,LOW);
 delay(time);
 digitalWrite(BtP,HIGH); 
}

void VupB(int time){  //Volume up Button pressing function
 digitalWrite(VupP,LOW);
 delay(time);
 digitalWrite(VupP,HIGH); 
}

void VdownB(int time){  //Volume down Button pressing function
 digitalWrite(VdownP,LOW);
 delay(time);
 digitalWrite(VdownP,HIGH); 
}

void Main_test_script(){
Total++;  //For counting the total test loop times
Test_init();  //Make sure the testing is start from power off state
//////////////////////////////////////////
PowerB(200);//Press power button to On
delay(500);
LED_Filter();//Should show bootload
delay(500);
Serial.println("System Bootload");
if (Status == "BootLoad") {
Serial.println("PASS");
SystemBootload++;}
else{Serial.println("FAIL");
SBF=SBF+Total+",";
SystemBootloadf++;}
//////////////////////////////////////////
delay(30000);
LED_Filter();//Should show Network connected
delay(500);
Serial.println("Power ON complete - Network connected");
if (Status == "Network connected") {
Serial.println("PASS");
NormalOperation1++;}
else{Serial.println("FAIL");
NO1F=NO1F+Total+",";
NormalOperation1f++;}
delay(500);
//////////////////////////////////////////

int y=random(1,5);  //This can be change base on the range require, the example is random from 1 to 5
Serial.print("Aux_Spotify_Loop ");Serial.print(y);Serial.println(" times");
for(int x=0;x<y;x++){
Aux();
Spotify();
init_det();
if (test_det=='N') return;
}

y=random(1,10);  //This can be change base on the range require, the example is random from 1 to 10
Serial.print("Vol-_Vol+_Loop ");Serial.print(y);Serial.println(" times");
for(int x=0;x<y;x++){
  int rxy=random(1,10);
  if (rxy>5) volume_up();
  else volume_down();
init_det();
if (test_det=='N') return;
}

PowerB(200);//Press power button to Sleep
delay(1500);
LED_Filter();//Should show Sleep
delay(500);
Serial.println("System Sleep");
if (Status == "Sleep") {
Serial.println("PASS");
SystemSleep++;}
else{Serial.println("FAIL");
SSF=SSF+Total+",";
SystemSleepf++;}
delay(500);
power_off();
}


void Test_init(){  //Make sure the testing is start from power off state
delay(5000);
Serial.println("------------------------------");
Serial.print("Test Start "); Serial.println(Total);
Serial.println("------------------------------");
Serial.print("Current Status = ");
LED_Filter();
Serial.println(Status);
if(Status != "Off"){
Serial.println("Test will force start from Power off.");
PowerB(6000);
delay(2000);
Serial.print("Current Status = ");
LED_Filter();
Serial.println(Status);
}
delay(500);
}

void power_off(){
PowerB(6000);
delay(2000);
LED_Filter();
delay(500);
Serial.println("System Off");
if (Status == "Off") {
Serial.println("PASS");
SystemPowerOff++;}
else{Serial.println("FAIL");
SPOF=SPOF+Total+",";
SystemPowerOfff++;}
delay(500);
}

void volume_up(){
vupcount++;
VupB(200);
delay(500);
Serial.println("Volume up ");
}

void volume_down(){
vdowncount++;
VdownB(200);
delay(500);
Serial.println("Volume down ");
}

void Spotify(){
BtB(200);//Press
delay(1000);
LED_Filter();//Should show Network connected
delay(500);
Serial.println("Switch to Spotify");
if (Status == "Network connected") {
Serial.println("PASS");
NormalOperation2++;}
else{Serial.println("FAIL");
NO2F=NO2F+Total+",";
NormalOperation2f++;}
delay(500);
}

void Aux(){
AuxB(200);//Press Aux
delay(1000);
LED_Filter();//Should show Aux
delay(500);
Serial.println("Switch to AUX");
if (Status == "AUX") {
Serial.println("PASS");
AUX++;}
else{Serial.println("FAIL");
AUF=AUF+Total+",";
AUXf++;}
delay(500);
}

void Summary() {
float tpass=SystemBootload+NormalOperation1+
                AUX+NormalOperation2+SystemSleep+SystemPowerOff;  //calculate the total pass item quantity
float tfail=SystemBootloadf+NormalOperation1f+
                AUXf+NormalOperation2f+SystemSleepf+SystemPowerOfff;  //calculate the total fail item quantity
Serial.println("------------------------------");
Serial.println("Summary                      |");
Serial.println("------------------------------");
Serial.print("% of Pass = "); 
Serial.print(tpass/(tpass+tfail)*100); 
Serial.println("%");
Serial.print("% of fail = "); 
Serial.print(tfail/(tpass+tfail)*100); 
Serial.println("%");
Serial.print("Number of Pass = "); 
Serial.println(int(tpass)); 
Serial.print("Number of Fails = "); 
Serial.println(int(tfail)); 
Serial.print("Number of loop run = "); 
Serial.println(Total); 
Serial.print("Number of action run = "); 
Serial.println(int(tpass+tfail+vupcount+vdowncount)); 

Serial.println("");
Serial.println("System Bootload"); 
Serial.print("PASS = "); Serial.println(SystemBootload); 
Serial.print("FAIL = "); Serial.println(SystemBootloadf);
if (SBF!="") {Serial.print("FAIL AT LOOP : ");Serial.println(SBF);}
Serial.println("Power ON complete - Network connected"); 
Serial.print("PASS = "); Serial.println(NormalOperation1); 
Serial.print("FAIL = "); Serial.println(NormalOperation1f);
if (NO1F!="") {Serial.print("FAIL AT LOOP : ");Serial.println(NO1F);}
Serial.println("Switch to AUX"); 
Serial.print("PASS = "); Serial.println(AUX); 
Serial.print("FAIL = "); Serial.println(AUXf);
if (AUF!="") {Serial.print("FAIL AT LOOP : ");Serial.println(AUF);}
Serial.println("Switch to Network connected"); 
Serial.print("PASS = "); Serial.println(NormalOperation2); 
Serial.print("FAIL = "); Serial.println(NormalOperation2f);
if (NO2F!="") {Serial.print("FAIL AT LOOP : ");Serial.println(NO2F);}

Serial.println("Volume Up");
Serial.print("Test = ");
Serial.println(vupcount);

Serial.println("Volume Down"); 
Serial.print("Test = ");
Serial.println(vdowncount);

Serial.println("System Sleep"); 
Serial.print("PASS = "); Serial.println(SystemSleep); 
Serial.print("FAIL = "); Serial.println(SystemSleepf);
if (SSF!="") {Serial.print("FAIL AT LOOP : ");Serial.println(SSF);}
Serial.println("System Off"); 
Serial.print("PASS = "); Serial.println(SystemPowerOff); 
Serial.print("FAIL = "); Serial.println(SystemPowerOfff);
if (SPOF!="") {Serial.print("FAIL AT LOOP : ");Serial.println(SPOF);}
Serial.println("------------------------------");
}
